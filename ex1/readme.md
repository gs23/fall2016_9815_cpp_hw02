## Usage
use make to compile the code:
```shell
> make
g++ -std=c++11 -pthread -W  -g	  -c -o master.o master.cpp 
g++ -std=c++11 -pthread -W  -g	  -o master master.o -lrt
g++ -std=c++11 -pthread -W  -g	  -c -o slave.o slave.cpp 
g++ -std=c++11 -pthread -W  -g	  -o slave slave.o -lrt
```
In terminal 1, execute "master":
```shell
> ./master
Type in the number wait to be published: 
```
In terminal 2, execute "slave":
```shell
>./slave
```

go back to terminal 1, type in any integer you want to publish to "slave":
```shell
> ./master
Type in the number wait to be published: 
2
```

you will find in terminal 2, "slave" receives what is published:
```shell
> ./slave
Value received: 2
```
you can keep typing integer in terminal 1 to publish integer from "master" to "slave"