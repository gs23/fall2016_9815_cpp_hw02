/**
    MTH 9815 hw2
    test.cpp
    Purpose: subscribing program to recieve integer from publishing program
    @author Hongshan Chu, Yinzhi Wang
*/


#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <iostream>
#include <boost/interprocess/sync/interprocess_semaphore.hpp>
#include "shared_memory_buffer.h"

using namespace boost::interprocess;


int main()
{
  
  //use created shared memory object 
  shared_memory_object shdmem{open_or_create, "MySharedMemory", read_write};

  //set the size of shared memory with size of memory buffer
  shdmem.truncate(sizeof(shared_memory_buffer));

  //map shared memory into the address space of a processs
  mapped_region region(shdmem,read_write);

  //get the region address
  void * addr = region.get_address();

  //assigned the address to the buffer pointer
  shared_memory_buffer * data = static_cast<shared_memory_buffer*>(addr);

  while(true){
      //wait until a number gets available
      data->reader.wait();

      //print the original value
      std::cout << "Value received: " << data->value << std::endl;


      //writer can print teh number
      data->writer.post();
   }
   return 0;

}