/**
    MTH 9815 hw2
    shared_memory_buffer.h
    Purpose: header file for shared_memory_buffer declaration and definition

    @author Hongshan Chu, Yinzhi Wang
*/

#ifndef MEM_BUFF

#define MEM_BUFF

#include <boost/interprocess/sync/interprocess_semaphore.hpp>

using namespace boost::interprocess;
struct shared_memory_buffer {
  //writer initialized with one to start
  //reader have to wait 
  shared_memory_buffer(): writer(1), reader(0), value(0){}

  interprocess_semaphore writer, reader;

  //integer value waited to be published to slave
  int value;
};
#endif