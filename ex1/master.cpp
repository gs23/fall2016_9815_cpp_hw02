/**
    MTH 9815 hw2
    master.cpp
    Purpose: parent program to publish integer

    @author Hongshan Chu, Yinzhi Wang
*/


#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <iostream>
#include <boost/interprocess/sync/interprocess_semaphore.hpp>
#include "shared_memory_buffer.h"

using namespace boost::interprocess;

int main()
{
  //create shared memory object with name, read and write access
  shared_memory_object shdmem{open_or_create, "MySharedMemory", read_write};

  //set the size of shared memory with size of memory buffer
  shdmem.truncate(sizeof(shared_memory_buffer));

  //map shared memory into the address space of a process
  mapped_region region(shdmem,read_write);

  //get the region address
  void * addr = region.get_address();

  //create a shared memory buffer in memory
  shared_memory_buffer *data = new (addr) shared_memory_buffer;

  while(true){

    //wait until the written number gets executed
    data->writer.wait();

    //print the square root of the original value
    std::cout << "Type in the number wait to be published: " << std::endl;

    //create the original value
    std::cin >> data->value;

    sleep(1);
    //reader can execute written number
    data->reader.post();
  }

}