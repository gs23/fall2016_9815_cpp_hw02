/**
    MTH 9815 hw2
    test.cpp
    Purpose: test program for our ProductServices

    @author Hongshan Chu, Yinzhi Wang
*/


#include <iostream>
#include "products.hpp"
#include "productservice.hpp"

using namespace std;

int main()
{
  // Create the 10Y treasury note
  date maturityDate(2026, Nov, 16);
  string cusip = "912828M56";
  Bond treasuryBond(cusip, CUSIP, "T", 2.25, maturityDate);

  // Create the 2Y treasury note
  date maturityDate2(2018, Nov, 5);
  string cusip2 = "912828TW0";
  Bond treasuryBond2(cusip2, CUSIP, "T", 0.75, maturityDate2);

  // Create a BondProductService
  BondProductService *bondProductService = new BondProductService();

  // Add the 10Y note to the BondProductService and retrieve it from the service
  bondProductService->Add(treasuryBond);
  //Bond bond = bondProductService->GetData(cusip);
  //cout << "CUSIP: " << bond.GetProductId() << " ==> " << bond << endl;

  // Add the 2Y note to the BondProductService and retrieve it from the service
  bondProductService->Add(treasuryBond2);
  //bond = bondProductService->GetData(cusip2);
  //cout << "CUSIP: " << bond.GetProductId() << " ==> " << bond << endl;

  // Create the Spot 10Y Outright Swap
  date effectiveDate(2016, Nov, 16);
  date terminationDate(2026, Nov, 16);
  string outright10Y = "Spot-Outright-10Y";
  IRSwap outright10YSwap(outright10Y, THIRTY_THREE_SIXTY, THIRTY_THREE_SIXTY, SEMI_ANNUAL, LIBOR, TENOR_3M, effectiveDate, terminationDate, USD, 10, SPOT, OUTRIGHT);

  // Create the IMM 2Y Outright Swap
  date effectiveDate2(2016, Dec, 20);
  date terminationDate2(2018, Dec, 20);
  string imm2Y = "IMM-Outright-2Y";
  IRSwap imm2YSwap(imm2Y, THIRTY_THREE_SIXTY, THIRTY_THREE_SIXTY, SEMI_ANNUAL, LIBOR, TENOR_3M, effectiveDate2, terminationDate2, USD, 2, IMM, OUTRIGHT);

  // Create a IRSwapProductService
  IRSwapProductService *swapProductService = new IRSwapProductService();

  // Add the Spot 10Y Outright Swap to the IRSwapProductService and retrieve it from the service
  swapProductService->Add(outright10YSwap);
  //IRSwap swap = swapProductService->GetData(outright10Y);
  //cout << "Swap: " << swap.GetProductId() << " == > " << swap << endl;

  // Add the IMM 2Y Outright Swap to the IRSwapProductService and retrieve it from the service
  swapProductService->Add(imm2YSwap);
  //swap = swapProductService->GetData(imm2Y);
  //cout << "Swap: " << swap.GetProductId() << " == > " << swap << endl;







  //test the newly written service method
  //*******************************************************************************

  cout << "Test GetBonds(string& _ticker): " << endl;  
  string test_ticker("T");
  vector<Bond> bond_vec = bondProductService->GetBonds(test_ticker);
  for(auto& ref: bond_vec){
      cout << "Bond: " << ref.GetProductId() << " == > " << ref << endl;  
  }
  cout << endl;



  cout << "Test GetSwaps(PaymentFrequency _fixedLegPaymentFrequency): " << endl;  
  vector<IRSwap> swap_vec = swapProductService->GetSwaps(SEMI_ANNUAL);
  for(auto& ref: swap_vec){
      cout << "Swap: " << ref.GetProductId() << " == > " << ref << endl;  
  }
  cout << endl;

  
  cout << "Test GetSwaps(DayCountConvention _fixedLegDayCountConvention): " << endl;  
  swap_vec = swapProductService->GetSwaps(THIRTY_THREE_SIXTY);
  for(auto& ref: swap_vec){
      cout << "Swap: " << ref.GetProductId() << " == > " << ref << endl;  
  }
  cout << endl;

  

  cout << "Test GetSwaps(FloatingIndex _floatingIndex): " << endl;  
  swap_vec = swapProductService->GetSwaps(LIBOR);
  for(auto& ref: swap_vec){
      cout << "Swap: " << ref.GetProductId() << " == > " << ref << endl;  
  }
  cout << endl;

  cout << "Test GetSwapsGreaterThan(int _termYears): " << endl;  
  swap_vec = swapProductService->GetSwapsGreaterThan(1);
  for(auto& ref: swap_vec){
      cout << "Swap: " << ref.GetProductId() << " == > " << ref << endl;  
  }
  cout << endl;

  cout << "Test GetSwapsLessThan(int _termYears): " << endl;  
  swap_vec = swapProductService->GetSwapsLessThan(5);
  for(auto& ref: swap_vec){
      cout << "Swap: " << ref.GetProductId() << " == > " << ref << endl;  
  }
  cout << endl;

  cout << "Test GetSwaps(SwapType _swapType): " << endl;  
  swap_vec = swapProductService->GetSwaps(IMM);
  for(auto& ref: swap_vec){
      cout << "Swap: " << ref.GetProductId() << " == > " << ref << endl;  
  }
  cout << endl;

  cout << "Test GetSwaps(SwapLegType _swapLegType): " << endl;  
  swap_vec = swapProductService->GetSwaps(OUTRIGHT);
  for(auto& ref: swap_vec){
      cout << "Swap: " << ref.GetProductId() << " == > " << ref << endl;  
  }

  return 0;
}
