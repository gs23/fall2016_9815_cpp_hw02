## File Content
* The required methods for BondProductService and IRSwapProductService were added in file:
productservice.hpp.

* test.cpp was created for testing those methods.

* Other file were mostly the same for sample file given in product.zip
## Usage
use make to compile the code:
```shell
> make
g++ -std=c++11  -W  -g	  -c -o test.o test.cpp 
g++ -std=c++11  -W  -g	  -o test test.o
```
In terminal, execute "test":
```shell
> ./test
```
