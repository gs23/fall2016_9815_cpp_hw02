/**
    MTH 9815 hw2
    futureproduct.h
    Purpose: implementation for future product

    @author Hongshan Chu, Yinzhi Wang
*/

#ifndef FUTURE_PROD
#define FUTURE_PROD

#include "./products/products.hpp"

#include <iostream>



using namespace std;

//Future Type
enum FutureType { EURODOLLARFUTURE, BONDFUTURE };


/**
 * Modeling a future product
 */

class Future : public Product {

public:
    // Future constructor    
    Future(string _productId, FutureType _futureType, string _ticker, date _maturityDate);
    Future();


  	// Return the ticker of the future
  	string GetTicker() const;

    // Return the future type
    FutureType GetFutureType() const;

    // Return the maturity date if the future
    date GetMaturityDate() const;

    // Overload the << operator to print out the future
    friend ostream &operator<<(ostream &output, const Future &future);

private:

    
    string ticker;// ticker variable
    date maturityDate; // maturity date variable
    FutureType futureType;//future type variable
};

/**
 * Modeling a bondfuture contract
 */

class BondFuture : public Future {

public:
	// Bondfuture constructor
    BondFuture(string _productId, string _ticker, date _maturityDate);
    BondFuture();

    // Overload the << operator to print out the bondfuture
    friend ostream &operator<<(ostream &output, const BondFuture &bondFuture);
private:

};

/**
 * Modeling a EuroDollarFuture contract
 */
class EuroDollarFuture : public Future {

public:
	// Bondfuture constructor
    EuroDollarFuture(string _productId, string _ticker, date _maturityDate);
    EuroDollarFuture();

    // Overload the << operator to print out the EuroDollarFuture
    friend ostream &operator<<(ostream &output, const EuroDollarFuture &euroDollarFuture);

private:

};


//******************************************************************
//future class method implementation:

//default constructor implementation for future
Future::Future() : Product("defualtID", FUTURE) {}

//constructor implementation for future
Future::Future(string _productId, FutureType _futureType, string _ticker, date _maturityDate): 
					Product(_productId, FUTURE),futureType(_futureType), maturityDate(_maturityDate), ticker(_ticker){}



//get the maturity date of future contract
date Future::GetMaturityDate() const { return maturityDate; }

//get the type of future contract
FutureType Future::GetFutureType() const { return futureType; }

//get the ticker of future contract
string Future::GetTicker() const { return ticker;}


//print out the future info
ostream &operator<<(ostream &output, const Future &Future) {
	output << Future.GetFutureType() << endl;
	output << Future.GetTicker() << endl;
    output << Future.GetMaturityDate();
    return output;
}

//******************************************************************


//******************************************************************
//BondFuture class method implementation:

//default constructor implementation for BondFuture
BondFuture::BondFuture() : Future() {}

//constructor implementation for BondFuture
BondFuture::BondFuture(string _productId, string _ticker, date _maturityDate)
    					: Future(_productId, BONDFUTURE, _ticker, _maturityDate) {}


//print out the future info
ostream &operator<<(ostream &output, const BondFuture &bondFuture) {
	output << bondFuture.GetFutureType() << endl;
	output << bondFuture.GetTicker() << endl;
    output << bondFuture.GetMaturityDate();
    return output;
}

//******************************************************************



//******************************************************************
//EuroDollarFuture class method implementation:


//default constructor implementation for EuroDollarFuture 
EuroDollarFuture::EuroDollarFuture(string _productId,string _ticker, date _maturityDate)
    								: Future(_productId, EURODOLLARFUTURE, _ticker, _maturityDate) {}
//default constructor implementation for BondFuture
EuroDollarFuture::EuroDollarFuture() : Future() {}

//print out the EuroDollarFuture info
ostream &operator<<(ostream &output, const EuroDollarFuture &euroDollarFuture) {

	output << euroDollarFuture.GetFutureType() << endl;
	output << euroDollarFuture.GetTicker() << endl;
    output << euroDollarFuture.GetMaturityDate();

    return output;
}

//******************************************************************
#endif
