## File Content
* The required Future class,EuroDollarFuture and BondFuture class were added in file:
futureproduct.h.

* The corresponding service implementation were in file futureproductservice.h.

* test.cpp was created for testing those methods and service.

* Original sample files from product.zip were in directory "product".
## Usage
use make to compile the code:
```shell
> make
g++ -std=c++11  -W  -g	  -c -o test.o test.cpp 
g++ -std=c++11  -W  -g	  -o test test.o 
```
In terminal, execute "test":
```shell
> ./test
```
