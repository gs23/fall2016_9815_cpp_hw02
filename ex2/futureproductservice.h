/**
    MTH 9815 hw2
    futureproductservice.h
    Purpose: implementation for future product service

    @author Hongshan Chu, Yinzhi Wang
*/

#include <iostream>
#include <map>
#include "futureproduct.h"
#include "./products/soa.hpp"

using namespace std;

class FutureProductService : public Service<string,Future>
{

public:
  // FutureProductService ctor
  FutureProductService();

  // Return the future data for a particular bond product identifier
  Future& GetData(string productId);

  // Add a future to the service (convenience method)
  void Add(Future &future);

private:
  map<string,Future> futureMap; // cache of future products

};

//implementation of constructor
FutureProductService::FutureProductService(){
	futureMap = map<string, Future>();
}


//get the future based on given ID
Future& FutureProductService::GetData(string productId){
	return futureMap[productId];
}


//insert future contract into the service std::map<key, value> map;
void FutureProductService::Add(Future &future){
	futureMap.insert(pair<string, Future>(future.GetProductId(),future));

}


