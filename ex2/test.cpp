/**
    MTH 9815 hw2
    test.cpp
    Purpose: test program for future ProductServices

    @author Hongshan Chu, Yinzhi Wang
*/

#include "futureproduct.h"
#include "futureproductservice.h"
#include <iostream>

int main(){

    //create EurodollarFuture contract
    date futureMature1(2016, Nov, 14);
    string future_ID1 = "EDX16";
    EuroDollarFuture euroDollarFuture1(future_ID1,"EURODF",futureMature1);

    //create EurodollarFuture contract
    date futureMature2(2016, Dec, 19);
    string future_ID2 = "EDZ16";
    EuroDollarFuture euroDollarFuture2(future_ID2,"EURODF",futureMature2);

    //create BondFuture contract
	date futureMature3(2017, Mar, 19);
    string future_ID3 = "17H17";
    BondFuture bondFuture1(future_ID3,"TBONDF",futureMature3);

    //create BondFuture contract
    date futureMature4(2016, Dec, 19);
    string future_ID4 = "17Z16";
    BondFuture bondFuture2(future_ID4,"TBONDF",futureMature4);



    // Create a FutureProductService
    FutureProductService *futureProductService = new FutureProductService();

    // Add the EuroDollarFuture contract to the futureProductService and retrieve it from the service
    futureProductService->Add(euroDollarFuture1);
    Future future = futureProductService->GetData(future_ID1);
    cout << "EuroDollarFuture: " << future.GetProductId() << " ==> " << future << endl;

    // Add the EuroDollarFuture contract to the futureProductService and retrieve it from the service
    futureProductService->Add(euroDollarFuture2);
    future = futureProductService->GetData(future_ID2);
    cout << "EuroDollarFuture: " << future.GetProductId() << " ==> " << future << endl;

	// Add the BondFuture contract to the futureProductService and retrieve it from the service
    futureProductService->Add(bondFuture1);
    future = futureProductService->GetData(future_ID3);
    cout << "EuroDollarFuture: " << future.GetProductId() << " ==> " << future << endl;

    // Add the BondFuture contract to the futureProductService and retrieve it from the service
    futureProductService->Add(bondFuture2);
    future = futureProductService->GetData(future_ID4);
    cout << "EuroDollarFuture: " << future.GetProductId() << " ==> " << future << endl;


}